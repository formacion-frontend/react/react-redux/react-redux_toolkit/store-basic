import { combineReducers, configureStore } from "@reduxjs/toolkit";
import colores from "./colores";

export const store = configureStore({
  reducer: combineReducers({
    colores
  })
});