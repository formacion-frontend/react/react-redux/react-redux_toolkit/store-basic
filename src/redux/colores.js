import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "coloresValue",
  initialState: {
    buttons: [
      {
        text: "Guardar",
        backgroundColor: "dodgerblue",
        color: "white"
      },
      {
        text: "Alquilar",
        backgroundColor: "gold",
        color: "white"
      },
      {
        text: "Añadir suscripción",
        backgroundColor: "coral",
        color: "white"
      }
    ]
  }
});

export default appSlice.reducer;