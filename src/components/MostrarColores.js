import React from 'react';
import { useSelector } from "react-redux";

const MostrarColores = () => {

    const data = useSelector((state) => state.colores.buttons);

    const mostrarColoresTXT = data.map((obj)=>(<li>{obj.backgroundColor}</li>))
    console.log(mostrarColoresTXT)
    

  return (
  <div>Colores
      <hr/>
      {mostrarColoresTXT}<br/>
  </div>);
};

export default MostrarColores;
